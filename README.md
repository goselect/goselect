# goselect

A command line tool to help manage Go toolchains. It can download, install, and
remove Go toolchains. It uses a proxy executable to emable easily switching
between installed toolchains.

## Copyright

This is copyrighted and made freely available under the terms of the MIT
license.