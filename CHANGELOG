# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.120 - 2024-09-13
### Changed
- Upgraded gitlab.com/goselect/gorelease to v0.2.0

## [0.0.11] - 2024-09-13
### Fixed
- Incorrect version string

## [0.0.10] - 2024-09-13
### Added
- Add gosem v0.1.1

### Changed
- Updated gotoolchain to v0.6.0
- Use gosem where appropriate

## [0.0.9] - 2024-05-07
### Fixed
- Wrong version reported for current and latest for check command.

## [0.0.8] - 2024-05-01
### Changed
- Improve error handling in check command.
- Rename install command to self-install and also add initial implementation.

## [0.0.7] - 2024-04-29
### Added
- New command "check" to retrieve the latest Go version and compare with the current.

## [0.0.6] - 2023
### Changed
- Updated year in License
- Exit with error message and code rather than panic
- Upgrade dependencies

## [0.0.5] - 2022-06-13
### Added
- Added GOSELECT_ROOT env for --root flag.
- Added GOSELECT_DOWNLOADS env for --downloads flag.
- Added GOSELECT_TOOLCHAINS env for --toolchains flag.

### Changed
- Added check for GOARM env when downloading or setting the version when the
  arch is `arm`. If GOARCH is 8 then the arch is changed to `arm64` otherwise it
  is `armv6l`.
- Updated `github.com/urfave/cli/v2` dependency version to v2.8.1.
- Updated `github.com/c2h5oh/datasize` dependency version to
  v0.0.0-20220606134207-859f65c6625b.

## [0.0.4] - 2022-04-09
### Added
- Added an `extract` flag to `download` command that will install a toolchain.
- Added usage text to `download` `archive` flag.

### Changed
- Updated `github.com/urfave/cli/v2` dependency version to v2.4.0.
- Updated `github.com/nlepage/go-tarfs` dependecy version to v1.1.1.

### Fixed
- Fixed bug that prevended downloading when the main set functionality was used.

## [0.0.3] - 2022-04-09
### Added
- Added (this) changelog.

### Changed
- Use `http` to download instead of `grab`.

## [0.0.2] - 2022-03-11
### Added
- Implemented `list` command.
- Implemented `current` command.
- Implemented `remove` command.
- Implemented `download` command.
- Implemented `licenses` command.

## [0.0.1] - 2022-03-23
### Added
- Created the repository.
