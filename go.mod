module gitlab.com/goselect/goselect

go 1.22

require (
	github.com/c2h5oh/datasize v0.0.0-20231215233829-aa82cc1e6500
	github.com/nlepage/go-tarfs v1.2.1
	github.com/urfave/cli/v2 v2.27.4
	gitlab.com/antizealot1337/progress v0.1.0
	gitlab.com/goselect/gorelease v0.2.0
	gitlab.com/goselect/gosem v0.1.1
	gitlab.com/goselect/gotoolchain v0.6.0
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.4 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/xrash/smetrics v0.0.0-20240521201337-686a1a2994c1 // indirect
)
