package main

import (
	"archive/zip"
	"compress/gzip"
	"context"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"

	"github.com/c2h5oh/datasize"
	"github.com/urfave/cli/v2"
	"gitlab.com/antizealot1337/progress"
	"gitlab.com/goselect/gorelease"
	"gitlab.com/goselect/goselect/licenses"
	"gitlab.com/goselect/gosem"
	"gitlab.com/goselect/gotoolchain"

	tarfs "github.com/nlepage/go-tarfs"
)

const DownloadURL = "https://go.dev/dl/"

const (
	ArchiveZip   = "zip"
	ArchiveTar   = "tar.gz"
	InstallerExe = "exe"
	InstallerPkg = "pkg"
)

const (
	FlagOS         = "os"
	FlagArch       = "arch"
	FlagRoot       = "root"
	FlagDownloads  = "downloads"
	FlagToolchains = "toolchains"
	FlagType       = "type"
	FlagVerbose    = "verbose"
	FlagURL        = "url"
	FlagExtract    = "extract"
)

const version = "0.0.12"

var (
	errNoVersionProvided = errors.New("no version provided")
)

func main() {
	cfg := gotoolchain.DefaultConfig()
	app := cli.App{
		Version: version,
		Usage:   "Manage Go toolchain versions",
		Flags: []cli.Flag{
			cli.BashCompletionFlag,
			&cli.StringFlag{
				Name:    FlagOS,
				Usage:   "Override GOOS for download/setting version",
				EnvVars: []string{"GOOS"},
				Value:   runtime.GOOS,
			},
			&cli.StringFlag{
				Name:    FlagArch,
				Usage:   "Override GOARCH for download/setting version",
				EnvVars: []string{"GOARCH"},
				Value:   runtime.GOARCH,
			},
			&cli.StringFlag{
				Name:        FlagRoot,
				Usage:       "Root directory of goselect",
				EnvVars:     []string{"GOSELECT_ROOT"},
				Value:       cfg.RootDir,
				Destination: &cfg.RootDir,
			},
			&cli.StringFlag{
				Name:        FlagDownloads,
				Usage:       "Downloads directory",
				EnvVars:     []string{"GOSELECT_DOWNLOADS"},
				Value:       cfg.DownloadDir,
				Destination: &cfg.DownloadDir,
			},
			&cli.StringFlag{
				Name:        FlagToolchains,
				Usage:       "Toolchains directory",
				EnvVars:     []string{"GOSELECT_TOOLCHAINS"},
				Value:       cfg.ToolchainDir,
				Destination: &cfg.ToolchainDir,
			},
			&cli.StringFlag{
				Name:    FlagURL,
				Aliases: []string{"u"},
				Usage:   "URL to download archives from",
				Value:   DownloadURL,
				EnvVars: []string{"GOSELECT_URL"},
			},
			&cli.BoolFlag{
				Name:    FlagVerbose,
				Aliases: []string{"V"},
				Usage:   "Increase output",
			},
		},
		Commands: []*cli.Command{
			{
				Name:    "list",
				Aliases: []string{"ls", "l"},
				Usage:   "List installed versions",
				Action: func(c *cli.Context) error {
					verbose := c.Bool(FlagVerbose)

					if verbose {
						fmt.Println("Listing the current toolchains at",
							cfg.ToolchainDir)
					} //if

					mgr := gotoolchain.NewManager(cfg)

					versions, err := mgr.ListVersions()
					if err != nil {
						return fmt.Errorf("list versions: %w", err)
					} //if

					for _, version := range versions {
						fmt.Println(version.String())
					} //for

					return nil
				},
			},
			{
				Name:    "current",
				Aliases: []string{"c"},
				Usage:   "Print the current version",
				Action: func(c *cli.Context) error {
					verbose := c.Bool(FlagVerbose)

					if verbose {
						fmt.Println("Reading current version from",
							cfg.VersionFile)
					} //if

					mgr := gotoolchain.NewManager(cfg)

					v, err := mgr.GetCurrentVersion()
					if err != nil {
						return fmt.Errorf("current version: %w", err)
					} //if

					fmt.Println(v.String())

					return nil
				},
			},
			{
				Name:  "check",
				Usage: "Check for the current version of Go",
				Action: func(ctx *cli.Context) error {
					var err error

					defer func() {
						if err != nil {
							err = fmt.Errorf("check: %w", err)
						}
					}()

					var releases []gorelease.Release
					releases, err = gorelease.GetReleases(context.Background())
					if err != nil {
						return err
					} //if

					latest, err := gosem.ParseVersion(releases[0].Version)
					if err != nil {
						return err
					} //if

					mgr := gotoolchain.NewManager(cfg)

					current, err := mgr.GetCurrentVersion()
					if err != nil {
						return err
					} //if

					if *latest == *current {
						fmt.Printf("Using current version (%s)\n", latest)
					} else {
						fmt.Printf("Newer version (%s) than installed (%s)\n", latest, current)
					} //if

					return nil
				},
			},
			{
				Name:    "download",
				Aliases: []string{"dl", "d"},
				Usage:   "Download an archive",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:    FlagType,
						Aliases: []string{"t"},
						Usage:   "The type of archive to download",
						Value:   "archive",
					},
					&cli.BoolFlag{
						Name:  FlagExtract,
						Usage: "Extract the archive after download",
						Value: false,
					},
				},
				Action: func(c *cli.Context) error {
					verbose := c.Bool(FlagVerbose)
					kind := c.String(FlagType)

					args := c.Args()
					if args.Len() == 0 {
						return errNoVersionProvided
					} //if

					mgr := gotoolchain.NewManager(cfg)

					goos := c.String(FlagOS)
					arch := c.String(FlagArch)

					if arch == "arm" {
						if os.Getenv("GOARM") == "8" {
							arch = "arm64"
						} else {
							arch = "armv6l"
						} //if
					} //if

					var ext string

					switch kind {
					case "source":
						ext = ArchiveTar
					case "archive":
						if goos == "windows" {
							ext = ArchiveZip
						} else {
							ext = ArchiveTar
						} //if
					case "installer":
						switch goos {
						case "windows":
							ext = InstallerExe
						case "darwin":
							ext = InstallerPkg
						default:
							ext = ArchiveTar
						} //switch
					} //switch

					url := c.String(FlagURL)

					for _, versionStr := range args.Slice() {
						v, err := gosem.ParseVersion(versionStr)
						if err != nil {
							return fmt.Errorf("version: %w", err)
						} //if

						archivename := fmt.Sprintf("go%s.%s-%s.%s",
							versionStr, goos, arch, ext)

						if strings.HasSuffix(url, "/") {
							url = url + archivename
						} else {
							url = url + "/" + archivename
						} //if

						if verbose {
							fmt.Println("Downloading", url)
						} else {
							fmt.Println("Downloading", versionStr)
						} //if

						archivepath := filepath.Join(cfg.DownloadDir,
							archivename)

						err = downloadArchive(archivepath, url, true)
						if err != nil {
							return fmt.Errorf("download: %w", err)
						} //if

						if !c.Bool(FlagExtract) {
							continue
						} //if

						if verbose {
							fmt.Println("Extracting", archivepath, "to",
								cfg.ToolchainDir)
						} else {
							fmt.Println("Extracting", archivename)
						} //if

						archive, closer, err := openArchive(archivepath)
						if err != nil {
							return err
						} //if

						err = mgr.InstallToolchainFS(archive, v)
						if err != nil {
							return err
						} //if

						closer.Close()
					} //for

					return nil
				},
			},
			{
				Name:    "remove",
				Aliases: []string{"rm", "r"},
				Usage:   "Remove toolcain versions",
				Action: func(c *cli.Context) error {
					verbose := c.Bool(FlagVerbose)

					args := c.Args()
					if args.Len() == 0 {
						return errNoVersionProvided
					} //if

					mgr := gotoolchain.NewManager(cfg)

					for _, versionStr := range args.Slice() {
						v, err := gosem.ParseVersion(versionStr)
						if err != nil {
							fmt.Println(err)
							continue
						} //if

						if verbose {
							fmt.Println("Removing", versionStr)
						} //if

						mgr.Remove(v)
					} //for

					return nil
				},
			},
			{
				Name:  "licenses",
				Usage: "Display licenses",
				Action: func(c *cli.Context) error {
					for _, l := range licenses.All {
						fmt.Println(l)
					} //for
					return nil
				},
			},
			{
				Name:   "self-install",
				Hidden: true,
				Action: func(c *cli.Context) error {
					var err error
					defer func() {
						if err != nil {
							err = fmt.Errorf("self-install: %w", err)
						} //if
					}()

					err = os.MkdirAll(cfg.DownloadDir, 0755)
					if err != nil {
						return err
					} //if

					err = os.MkdirAll(cfg.ToolchainDir, 0755)
					if err != nil {
						return err
					} //if

					return nil
				},
			},
		},
		Action: func(c *cli.Context) error {
			verbose := c.Bool("verbose")
			switch c.Args().Len() {
			case 0:
				return cli.ShowAppHelp(c)
			case 1:
				v, err := gosem.ParseVersion(c.Args().First())
				if err != nil {
					return errors.New("bad version or command provided")
				} //if

				mgr := gotoolchain.NewManager(cfg)

				found, err := mgr.Contains(v)
				if err != nil {
					return err
				} //if

				if !found {
					goos := c.String(FlagOS)
					arch := c.String(FlagArch)
					ext := ArchiveTar

					if goos == "windows" {
						ext = ArchiveZip
					} //if

					if arch == "arm" {
						if os.Getenv("GOARM") == "8" {
							arch = "arm64"
						} else {
							arch = "armv6l"
						} //if
					} //if

					archivename := fmt.Sprintf("go%s.%s-%s.%s", v.String(),
						goos, arch, ext)
					url := c.String(FlagURL)

					if strings.HasSuffix(url, "/") {
						url = url + archivename
					} else {
						url = url + "/" + archivename
					} //if

					if verbose {
						fmt.Println("Downloading", url)
					} else {
						fmt.Println("Downloading", archivename)
					} //if

					err = downloadArchive(
						filepath.Join(cfg.DownloadDir, archivename),
						url,
						verbose,
					)
					if err != nil {
						return err
					} //if

					if verbose {
						fmt.Println("Installing to", cfg.ToolchainDir)
					} else {
						fmt.Println("Installing")
					} //if

					archive, closer, err := openArchive(
						filepath.Join(cfg.DownloadDir, archivename),
					)
					if err != nil {
						return err
					} //if
					defer closer.Close()

					err = mgr.InstallToolchainFS(archive, v)
					if err != nil {
						return err
					} //if
				} //if

				err = mgr.SetCurrentVersion(v)
				if err != nil {
					return err
				} //if

				if verbose {
					fmt.Println("Setting version to", v.String())
				} //if
			default:
				return errors.New("only one version allowed to set")
			} //switch

			return nil
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		fmt.Fprintln(os.Stderr, "error:", err)
		os.Exit(1)
	} //if
} //func

func downloadArchive(dest, url string, showProgress bool) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("download archive: %w", err)
		} //if
	}() //func

	resp, err := http.Get(url)
	if err != nil {
		return
	} //if
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		err = fmt.Errorf("http status %s", resp.Status)
		return
	} //if

	f, err := os.Create(dest)
	if err != nil {
		return
	} //if
	defer f.Close()

	pw := progress.NewWriter(f)
	if showProgress {
		size, _ := strconv.Atoi(resp.Header.Get("Content-Length"))

		const block = "                                        \r"

		pw.Callback = func(i int) {
			fmt.Print(block)

			if size == 0 {
				fmt.Printf("%s\r", datasize.ByteSize(i).HumanReadable())
				return
			} else {
				fmt.Printf("%s / %s %02.2f%%\r",
					datasize.ByteSize(i).HumanReadable(),
					datasize.ByteSize(size).HumanReadable(),
					float64(i)/float64(size)*100,
				)
			} //if
		} //func
	} //if
	_, err = io.Copy(pw, resp.Body)

	return
} //func

func openArchive(path string) (archive fs.FS, closer io.Closer, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("open archive: %w", err)
		} //if
	}() //func

	ext := filepath.Ext(path)

	if ext == "."+ArchiveZip {
		archive, err = zip.OpenReader(path)
		if err != nil {
			return
		} //if
		closer = archive.(io.Closer)
	} else {
		var f *os.File
		f, err = os.Open(path)
		if err != nil {
			return
		} //if
		closer = f

		var g *gzip.Reader
		g, err = gzip.NewReader(f)
		if err != nil {
			return
		} //if

		archive, err = tarfs.New(g)
		if err != nil {
			return
		} //if
	} //if

	return
} //func
